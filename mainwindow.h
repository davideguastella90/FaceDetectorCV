#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSharedPointer>
#include <QtMultimedia/qcamerainfo.h>
#include <QCamera>

#include <QStandardPaths>
#include <QFileDialog>
#include <QMessageLogger>
#include <QCameraImageCapture>
#include <QListWidgetItem>

#include <QTime>



#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "camerainfo.h"
#include "webcamgrabber.h"
#include "Detector/facedetectionutils.h"
#include "Detector/featuresdetector.h"
#include "adddetectordialog.h"

#include <QMutex>
#include <QQueue>
#include <QPainter>
#include <QPen>
using namespace cv;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_refreshDeviceListPushButton_clicked();

    void on_deviceListComboBox_currentIndexChanged(int index);

    void on_startCameraPushButton_clicked();

    void onFrameGrabbed(const QImage image);


    void on_resolutionsListWidgets_itemClicked(QListWidgetItem *item);

    void on_loadFaceDetectorFeaturesPushButton_clicked();

    void on_startFaceDetectionPushButton_clicked();

    void on_stopFaceDetectionPushButton_clicked();

    void on_saveDetectionPushButton_clicked();

    void on_addDetectorPushButton_clicked();

    void on_addDetector(const QString &name, const QString &featureFname, const QColor &color, bool activateNow);

private:
    Ui::MainWindow *ui;
    QList<CameraInfo*> cameraList;
    CameraInfo* selectedCamera;
    QSharedPointer<WebcamGrabber> grabber;

    QMutex mutex;
    QMutex eyesMutex;

    QQueue<QList<QRect>> faces;
    QQueue<QList<QRect>> eyes;
    QList<QRect> currentFaces;
    QList<QRect> currentEyes;
    QPen pen;
    QPen eyePen;


    QString featuresFileName;
    FeaturesDetector* detector;
    FeaturesDetector* eyesDetector;


    void startEyesDetector();

    bool saveNextDetection;

};

#endif // MAINWINDOW_H
