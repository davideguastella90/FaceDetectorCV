#ifndef CANNYDETECTOR_H
#define CANNYDETECTOR_H

#include <QObject>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "iostream"

using namespace cv;
using namespace std;

class CannyDetector
{
public:
    CannyDetector();


    static Mat CannyThreshold(Mat src)
    {
        Mat gray, edge, draw;
        cvtColor(src, gray, CV_BGR2GRAY);

        Canny( gray, edge, 50, 150, 3);


        cvtColor(edge, draw, CV_GRAY2BGR);

        //edge.convertTo(draw, CV_8UC3);

        return draw;
    }

};

#endif // CANNYDETECTOR_H
