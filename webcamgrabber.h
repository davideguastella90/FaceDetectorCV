#ifndef WEBCAMGRABBER_H
#define WEBCAMGRABBER_H

#include <QObject>
#include <QThread>
#include <QSize>
#include "imageutils.h"
#include "opencv2/opencv.hpp"
#include "opencv2/videoio.hpp"

using namespace cv;

class WebcamGrabber : public QThread
{
    Q_OBJECT

public:
    WebcamGrabber(int deviceID);
    WebcamGrabber(int deviceID, QSize resolution);

    void run() Q_DECL_OVERRIDE;
    void stopGrabbing();

    QSize getResolution() const;
    void setResolution(QSize resolution);
    void setDeviceID(int deviceID);

private:
    VideoCapture cap;
    QSize resolution;
    int deviceID;
    bool stop;
    bool restart;

signals:
    void grabbedFrame(const QImage frame);
};

#endif // WEBCAMGRABBER_H
