#include "webcamgrabber.h"

WebcamGrabber::WebcamGrabber(int deviceID, QSize resolution)
{
    this->deviceID = deviceID;
    this->resolution = resolution;
    stop = false;
    restart = false;
}

WebcamGrabber::WebcamGrabber(int deviceID) : WebcamGrabber(deviceID, QSize(640,480))
{ }

void WebcamGrabber::stopGrabbing()
{
    stop = true;
}

void WebcamGrabber::setDeviceID(int deviceID)
{
    this->deviceID = deviceID;
    restart = true;
}

QSize WebcamGrabber::getResolution() const
{
    return QSize(cap.get(CV_CAP_PROP_FRAME_WIDTH), cap.get(CV_CAP_PROP_FRAME_HEIGHT));
}

void WebcamGrabber::setResolution(QSize resolution)
{
    this->resolution = QSize(resolution);
    restart = true;
}

void WebcamGrabber::run()
{
    cap = VideoCapture(this->deviceID);
    if(!cap.isOpened())
        return;

    cap.set(CV_CAP_PROP_FRAME_WIDTH, resolution.width());
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, resolution.height());

    for(;;)
    {
        if(stop)
        {
            cap.release();
            stop = false;
            break;

        }
        if(restart)
        {
            cap.release();
            cap = VideoCapture(this->deviceID); // open the default camera
            if(!cap.isOpened())
                return;

            cap.set(CV_CAP_PROP_FRAME_WIDTH, resolution.width());
            cap.set(CV_CAP_PROP_FRAME_HEIGHT, resolution.height());
            restart = false;
        }
        Mat frame;
        cap >> frame; // get a new frame from camera

        if(!frame.data)
        {
            continue;
        }

        QImage im = ImageUtils::Mat2QImage(frame);
        emit grabbedFrame(im);

        if(waitKey(30) >= 0) break;
    }
}
